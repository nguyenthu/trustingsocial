# TrustingSocial guideline

1. Technical overview
- Architecture: Clean Architecture
- Dependency Injection : Dagger 2
- Multi-thread: RxJava
- Networking: OkHttp, Retrofit
- Image loader: Glide
- Create json files at /assets folder to mock response data

2. Build/Run
- Open project and wait for syncing it by gradle
- Attach the devices or emulators then click "Run" button on Android Studio

3. Unit Test
- Commonly you should write Unit tests in "test" folder (only run on JVM) and "androidTest" folder (UI test)
but I only write them on "test" folder because I used the "Robolectric" library which supports both JVM test and Android test
Moreover, by applying Robolectric, you don't need to attach any device or emulator. It's so cool!!!
- I also integrate "jacoco" tool to the project. It runs unit test and report the result as well as the level of coverage
in exported visual html
- Just open the "Terminal" tab on the bottom bar of Android Studio and type "./gradlew clean jacocoTestReport"
- The output coverage report is at the location "/build/coverage-report/index.html"
and the test result is at "/app/build/reports/tests/testDebugUnitTest/index.html"
