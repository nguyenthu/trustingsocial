package trustingsocial.finance.presentation.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

/**
 * This class is to handle the lifecycle of ViewModel and Observer
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
open class ActivityViewModel : ViewModel(), LifecycleObserver {

    @Volatile
    var mViewWeakReference: WeakReference<BaseView?>? = null

    var compositeDisposables: CompositeDisposable? = null

    var isLoadingObserver = ObservableField<Boolean>(false)
    /**
     * Create a common view from a weak reference
     *
     * @return a common view
     */
    inline fun <reified V : BaseView> view(): V? {
        if (mViewWeakReference == null || mViewWeakReference?.get() == null)
            return null
        return V::class.java.cast(mViewWeakReference?.get())
    }

    /**
     * Attach the observer to own life cycle
     *
     * @param view to create a weak reference
     */
    open fun onAttached(view: BaseView?) {
        mViewWeakReference = WeakReference(view)
        view?.lifecycleOwner()?.lifecycle?.addObserver(this)
    }

    /**
     * Init the composite of disposable when a life cycle created
     *
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    open fun onCreated() {
        if (compositeDisposables == null)
            compositeDisposables = CompositeDisposable()
    }

    /**
     * Remove the observer of the life cycle when it is destroyed
     *
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        val view = mViewWeakReference?.get()
        view?.lifecycleOwner()?.lifecycle?.removeObserver(this)
        if(compositeDisposables?.isDisposed == false) {
            compositeDisposables?.dispose()
        }
    }

    /**
     * Clear all the items of composite disposables
     */
    override fun onCleared() {
        super.onCleared()
        compositeDisposables?.dispose()
    }

    /**
     * Add a disposable to a list
     *
     * @param disposable
     */
    fun addDisposable(disposable: Disposable) {
        compositeDisposables?.add(disposable)
    }
}