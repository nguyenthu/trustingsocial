package trustingsocial.finance.presentation.viewmodel

import androidx.lifecycle.LifecycleOwner

/**
 * Base class of a view in MVVM
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
interface BaseView {
    fun lifecycleOwner(): LifecycleOwner
}