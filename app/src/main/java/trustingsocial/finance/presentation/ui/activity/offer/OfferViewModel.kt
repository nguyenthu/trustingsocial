package trustingsocial.finance.presentation.ui.activity.offer

import androidx.databinding.ObservableField
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.domain.usecase.GetOfferUseCase
import trustingsocial.finance.presentation.viewmodel.ActivityViewModel

/**
 * This view model class for Offer to process directly request from UI
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 * @property getOfferUseCase the object to process getting offer
 * @property isLoadingObserver the observable object for loading
 */
class OfferViewModel : ActivityViewModel() {
    lateinit var getOfferUseCase: GetOfferUseCase
    /**
     * Setup the use case
     */
    fun setAttributes(getOfferUseCase: GetOfferUseCase){
        this.getOfferUseCase = getOfferUseCase
    }

    /**
     * Get the offer info from api
     */
    fun getOffer(){
        val offerView: OfferView? = view()
        addDisposable(
            getOfferUseCase.getOffer()
                .doOnSubscribe { isLoadingObserver.set(true) }
                .doFinally { isLoadingObserver.set(false) }
                .subscribe({ offer ->
                    offerView?.onOfferLoaded(offer)
                }, {
                })
        )
    }
}