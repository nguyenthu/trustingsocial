package trustingsocial.finance.presentation.ui.activity.loan

import dagger.Component
import trustingsocial.finance.di.application.ApplicationComponent
import trustingsocial.finance.di.application.OutOfApplicationScope

/**
 * This component class is for making a loan feature
 * This requires the component of an App
 *
 * @author Thu Nguyen
 */
@Component(dependencies = [ApplicationComponent::class])
@OutOfApplicationScope
interface LoanComponent {
    fun inject(activity: LoanActivity)
}