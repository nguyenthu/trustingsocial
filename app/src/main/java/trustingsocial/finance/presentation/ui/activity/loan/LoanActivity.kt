package trustingsocial.finance.presentation.ui.activity.loan

import android.os.Bundle
import android.widget.Toast
import dagger.Component
import kotlinx.android.synthetic.main.activity_loans.*
import trustingsocial.finance.App
import trustingsocial.finance.R
import trustingsocial.finance.databinding.ActivityLoansBinding
import trustingsocial.finance.domain.usecase.GetOfferUseCase
import trustingsocial.finance.domain.usecase.GetProvinceUseCase
import trustingsocial.finance.domain.usecase.MakeLoanUseCase
import trustingsocial.finance.presentation.ui.activity.BaseActivity
import javax.inject.Inject

/**
 * This activity is to process the loan info for a customer
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
class LoanActivity : BaseActivity<ActivityLoansBinding, LoanViewModel, LoanComponent>(), LoanView {

    /**
     * Define the main resource id for the layout
     */
    override fun getLayoutId(): Int {
        return R.layout.activity_loans
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Catch the listener
        btSubmit.setOnClickListener{
            // Process a loan if every input is valid
            if(validateInput()){
                viewModel?.processLoan(etPhoneNumber.getInputText(), etFullName.getInputText(),
                    etIdNumber.getInputText(), etIncome.getInputText().toLong(), ddAddress.getSelectedText())
            }
        }

        // Get the province list
        viewModel?.getProvinceList()
    }

    /**
     * Create a dagger component for Loan
     */
    override fun createComponent(): LoanComponent? {
        return DaggerLoanComponent.builder()
            .applicationComponent(App.get(this@LoanActivity)!!.component())
            .build()
    }

    /**
     * Get the ViewModel of Loan
     */
    override fun getViewModelClass(): Class<LoanViewModel>? {
        return LoanViewModel::class.java
    }

    /**
     * Attach the UseCase to view model
     */
    @Inject
    fun setViewModelAttribute(getProvinceUseCase: GetProvinceUseCase, makeLoanUseCase: MakeLoanUseCase) {
        viewModel?.setAttributes(getProvinceUseCase, makeLoanUseCase)
    }

    /**
     * Inject the component for this activity
     */
    override fun onInject(component: LoanComponent) {
        component.inject(this@LoanActivity)
    }

    /**
     * Validate the info which a customer inputs on
     */
    fun validateInput(): Boolean{
        var isPhoneValid = etPhoneNumber.validateInput()
        var isFullNameValid = etFullName.validateInput()
        var isIdNumberValid = etIdNumber.validateInput()
        var isIncomeValid = etIncome.validateInput()
        var isAddressValid = ddAddress.validateInput()
        return  isPhoneValid &&  isFullNameValid && isIdNumberValid
                 && isIncomeValid && isAddressValid
    }

    /**
     * Process a load when it has been made
     *
     */
    override fun onLoanMade() {
        // Show a toast
        Toast.makeText(this, getString(R.string.loan_submit_success), Toast.LENGTH_LONG).show()
        finish()
    }

    /**
     * Update to province list after loaded
     */
    override fun onProvinceLoaded(provinceList: ArrayList<String>?) {
        if(provinceList != null) {
            ddAddress.setData(provinceList)
        }
    }
}