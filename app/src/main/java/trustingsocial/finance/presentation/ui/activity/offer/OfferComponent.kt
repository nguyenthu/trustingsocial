package trustingsocial.finance.presentation.ui.activity.offer

import dagger.Component
import trustingsocial.finance.di.application.ApplicationComponent
import trustingsocial.finance.di.application.OutOfApplicationScope

/**
 * This component class is for getting an offer feature
 * This requires the component of an App
 *
 * @author Thu Nguyen
 */
@Component(dependencies = [ApplicationComponent::class])
@OutOfApplicationScope
interface OfferComponent {
    fun inject(activity: OfferActivity)
}