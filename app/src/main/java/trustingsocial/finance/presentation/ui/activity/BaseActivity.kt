package trustingsocial.finance.presentation.ui.activity

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProviders
import dagger.Component
import kotlinx.android.synthetic.main.activity_loans.*
import trustingsocial.finance.presentation.viewmodel.ActivityViewModel
import trustingsocial.finance.presentation.viewmodel.BaseView

/**
 * This base activity is built for some common
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
abstract class BaseActivity<B : ViewDataBinding, VM : ActivityViewModel, Component> : AppCompatActivity(), BaseView {

    protected lateinit var viewDataBinding: B

    var viewModel: VM? = null

    private var mComponent: Component? = null
    /**
     * The abstract method to get resource id of layout
     *
     * @return resource id as Integer
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    open fun getViewModelClass(): Class<VM>? = null

    /**
     * Initialize the data binding and view model
     * Also decide to make screen full or not
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        // Initialize the data binding object
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())

        // Create viewModel and attach it
        if (getViewModelClass() != null)
            viewModel = ViewModelProviders.of(this).get(getViewModelClass()!!)
        viewModel?.onAttached(this)

        if(toolbar != null) {
            // Set up toolbar
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        mComponent = createComponent()
        mComponent?.let { onInject(it) }
    }

    /**
     * Abstract method for creating a new component
     */
    abstract fun createComponent(): Component?

    /**
     * Abstract method for injecting the component
     */
    abstract fun onInject(component: Component)

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> onBackPressed()
        }
        return true
    }
    /**
     * Clear the data binding object when the activity passed away
     *
     */
    override fun onDestroy() {
        viewDataBinding.unbind()
        super.onDestroy()
    }

    /**
     * Get the life cycle owner, that's itself
     * It is for Live data
     */
    override fun lifecycleOwner(): LifecycleOwner {
        return this
    }
}