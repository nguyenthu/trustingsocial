package trustingsocial.finance.presentation.ui.widget.dropdown

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import trustingsocial.finance.R
import android.widget.TextView

/**
 * This custom view for a dropdown
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 * @property tvTitle the holder control
 * @property spinner the dropdown list
 * @property tvError the error control
 * @property hint the placeholder text
 * @property isRequired the flag to know this field is required not empty
 * @property emptyError the error text
 * @property listItem the item list for dropdown
 */
open class CustomDropdown : LinearLayout, AdapterView.OnItemSelectedListener {

    private var tvTitle: TextView
    private var spinner: Spinner
    private var tvError: TextView

    var hint: String?
    var isRequired: Boolean
    var emptyError: String?

    var listItem = ArrayList<String>()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        // Read from resource
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomDropdown, defStyleAttr, 0)
        hint = a.getString(R.styleable.CustomDropdown_dd_hint)
        isRequired = a.getBoolean(R.styleable.CustomDropdown_dd_is_required, true)
        emptyError = a.getString(R.styleable.CustomDropdown_dd_empty_error)

        a.recycle()

        // Inflate the controls from layout
        orientation = VERTICAL
        LayoutInflater.from(context).inflate(R.layout.custom_dropdown, this, true)
        tvTitle = findViewById(R.id.tvTitle)
        spinner = findViewById(R.id.sp)
        tvError = findViewById(R.id.tvError)

        init()
    }

    /**
     * Fill the value to control
     */
    private fun init() {
        tvTitle.text = hint
        tvError.text = emptyError
        tvError.visibility = View.INVISIBLE

        // Setup the onclick from the title
        tvTitle.setOnClickListener { spinner.performClick() }
    }

    /**
     * Fill the item list to dropdown
     */
    fun setData(listItem: ArrayList<String>) {
        if(!listItem.isEmpty()) {
            this.listItem.clear()
            this.listItem.addAll(listItem)
            // Add hint as the last item
            if(hint != null) {
                this.listItem.add(hint!!)
            }else{
                this.listItem.add("")
            }
            // Create an ArrayAdapter using a simple spinner layout and languages array
            val arrayAdapter = CustomArrayAdapter(context, this.listItem)
            // Set layout to use when the list of choices appear
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
            spinner.setAdapter(arrayAdapter)
            spinner.setSelection(arrayAdapter.count)
            spinner.onItemSelectedListener = this
        }
    }

    /**
     * Get the actual selected text for a user
     */
    fun getSelectedText(): String {
        if (tvTitle.text.equals(hint)) {
            return ""
        }
        return tvTitle.text.toString()
    }

    /**
     * Decide to show/hide the error control
     */
    private fun setError() {
        tvError.visibility = when (isRequired) {
            true -> VISIBLE
            else -> View.INVISIBLE
        }
    }

    /**
     * Check the invalidation to ensure the selected text is not empty
     */
    private fun isValid(): Boolean {
        return !TextUtils.isEmpty(getSelectedText())
    }

    /**
     * Begin to validate the input
     */
    fun validateInput(): Boolean {
        // Reset the tvError first
        tvError.visibility = View.INVISIBLE
        if (isValid()) {
            return true
        }
        setError()
        return false
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    /**
     * Update the text value and text color when an item is selected
     */
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        tvTitle.text = listItem[position]
        if(tvTitle.text.toString().equals(hint)){
            tvTitle.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray))
        }else{
            tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
        }

    }

    /**
     * The array adapter to show an item on UI
     */
    class CustomArrayAdapter(context: Context , objects: MutableList<String>, resource: Int = android.R.layout.simple_spinner_item) :
        ArrayAdapter<String>(context, resource, objects) {

        /**
         * Get the item view from a position
         */
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val v = super.getView(position, convertView, parent)
            if (position == count) {
                v.findViewById<TextView>(android.R.id.text1).text = ""
                v.findViewById<TextView>(android.R.id.text1).hint = getItem(count)
            }

            return v
        }

        /**
         * Get the size of adapter, ignoring the last item as a hint
         */
        override fun getCount(): Int {
            return super.getCount() - 1
        }

    }
}