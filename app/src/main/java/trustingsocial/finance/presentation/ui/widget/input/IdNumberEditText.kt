package trustingsocial.finance.presentation.ui.widget.input

import android.content.Context
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import trustingsocial.finance.utils.Utils
import android.text.InputFilter



/**
 * This EditText for special id number
 * ensure that the type has only numbers
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
class IdNumberEditText : CustomEditText {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        // Only number for this field
        inputText.inputType = InputType.TYPE_CLASS_NUMBER

        // Permit maximum length of 12
        inputText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(12))
    }


    /**
     * An Id number only has the length of 9 or 12
     */
    override fun isValid(): Boolean {
        return TextUtils.isEmpty(getInputText()) || Utils.checkIdNumber(getInputText())
    }
}