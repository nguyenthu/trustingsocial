package trustingsocial.finance.presentation.ui.widget.input

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import kotlin.math.absoluteValue

/**
 * This EditText for special income/salary
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
class IncomeEditText : MoneyEditText {
    companion object {
        const val MINIMUM_INCOME = 3000000L
    }
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**
     * The validation ensures that the value is more than or equal the MINIMUM_INCOME
     */
    override fun isValid(): Boolean {
        val incomeValue = getInputText().toLongOrNull()
        if(incomeValue != null){
            return incomeValue.absoluteValue >= MINIMUM_INCOME
        }
        return false
    }
}