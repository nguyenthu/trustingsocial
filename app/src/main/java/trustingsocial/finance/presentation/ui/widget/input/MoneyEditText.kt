package trustingsocial.finance.presentation.ui.widget.input

import android.content.Context
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import trustingsocial.finance.extension.formatMoneyVND
import kotlin.math.absoluteValue

/**
 * This EditText for special money
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
open class MoneyEditText : CustomEditText {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        // Only number for this field
        inputText.inputType = InputType.TYPE_CLASS_NUMBER

        // Permit maximum length of 20
        inputText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(20))


        // Adjust display format while user is typing
        inputText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputText.removeTextChangedListener(this)
                val cleanString = s.toString().replace(".", "")
                val amount = cleanString.toDoubleOrNull()
                if(amount != null) {
                    val current = amount.formatMoneyVND()
                    inputText.setText(current)
                    inputText.setSelection(current.length)
                }
                inputText.addTextChangedListener(this)
            }
        })
    }

    /**
     * Get the selected text without the dot "." and comma "," symbols
     */
    override fun getInputText(): String{
        return super.getInputText().replace(".", "").replace(",", "")
    }
}