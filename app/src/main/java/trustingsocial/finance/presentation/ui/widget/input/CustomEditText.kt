package trustingsocial.finance.presentation.ui.widget.input

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import trustingsocial.finance.R

/**
 * This custom view for an edit text
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 * @property inputText The EditText for a user to type
 * @property tvError the TextView for showing an error
 * @property hint the placeholder text
 * @property isRequired the flag to know this field is required not empty
 * @property emptyError the error text when the control is empty
 * @property formatError the error text when the format is invalid
 */
open class CustomEditText : LinearLayout {
    var inputText: EditText
    var tvError: TextView

    var hint: String?
    var isRequired: Boolean
    var emptyError: String?
    var formatError: String?

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        // Read from resource
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, defStyleAttr, 0)
        hint = a.getString(R.styleable.CustomEditText_et_hint)
        isRequired = a.getBoolean(R.styleable.CustomEditText_et_is_required, true)
        emptyError = a.getString(R.styleable.CustomEditText_et_empty_error)
        formatError = a.getString(R.styleable.CustomEditText_et_format_error)

        a.recycle()

        // Inflate the controls from layout
        orientation = VERTICAL
        LayoutInflater.from(context).inflate(R.layout.custom_edittext, this, true)
        inputText = findViewById(R.id.et)
        tvError = findViewById(R.id.tvError)

        init()
    }

    /**
     * Fill the value to control
     */
    private fun init() {
        inputText.hint = hint
    }

    /**
     * Decide which error will show
     */
    private fun setError() {
        if (isRequired) {
            if (inputText.text.isBlank()) {
                tvError.text = emptyError
            } else {
                tvError.text = formatError
            }
        } else {
            if (inputText.text.isBlank()) {
                tvError.text = ""
            } else {
                tvError.text = formatError
            }
        }
    }

    /**
     * Check the validation of a control
     * to ensure that it is not empty
     */
    open fun isValid(): Boolean {
        if (isRequired) {
            if (inputText.text.isBlank()) {
                return false
            }
        }
        return true
    }

    /**
     * Process the validation of an input
     */
    fun validateInput(): Boolean {
        // Reset the error first
        tvError.text = ""
        if (isValid()) {
            return true
        }
        setError()
        return false
    }

    /**
     * Get the actual value for user
     */
    open fun getInputText(): String = inputText.text.toString().trim()
}