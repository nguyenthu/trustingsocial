package trustingsocial.finance.presentation.ui.activity.loan

import trustingsocial.finance.presentation.viewmodel.BaseView

/**
 * This class is a view in MVVM to define the Loan callback
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
interface LoanView : BaseView {
    fun onLoanMade()
    fun onProvinceLoaded(provinceList: ArrayList<String>?)
}