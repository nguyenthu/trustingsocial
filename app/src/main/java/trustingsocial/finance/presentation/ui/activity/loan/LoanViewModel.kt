package trustingsocial.finance.presentation.ui.activity.loan

import trustingsocial.finance.domain.usecase.GetProvinceUseCase
import trustingsocial.finance.domain.usecase.MakeLoanUseCase
import trustingsocial.finance.presentation.viewmodel.ActivityViewModel

/**
 * This view model class for Loan to process directly request from UI
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 * @property getProvinceUseCase the object to process getting province
 * @property makeLoanUseCase the object to process making loan
 */
class LoanViewModel : ActivityViewModel() {
    lateinit var getProvinceUseCase: GetProvinceUseCase
    lateinit var makeLoanUseCase: MakeLoanUseCase

    /**
     * Setup the use case
     */
    fun setAttributes(getProvinceUseCase: GetProvinceUseCase, makeLoanUseCase: MakeLoanUseCase){
        this.getProvinceUseCase = getProvinceUseCase
        this.makeLoanUseCase = makeLoanUseCase
    }

    /**
     * Get the province list from api
     */
    fun getProvinceList(){
        val loanView: LoanView? = view()
        addDisposable(
            getProvinceUseCase.getProvinceList()
                .subscribe({ provinceResponse ->
                    loanView?.onProvinceLoaded(provinceResponse.provincesList)
                }, {
                })
        )
    }

    /**
     * Make a loan
     */
    fun processLoan(phone: String, fullName: String, idNumber: String, salary: Long, province: String){
        val loanView: LoanView? = view()
        addDisposable(
            makeLoanUseCase.makeLoans(phone, fullName, idNumber, salary, province)
                .subscribe({
                    loanView?.onLoanMade()
                }, {
                })
        )
    }
}