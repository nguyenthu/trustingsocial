package trustingsocial.finance.presentation.ui.activity.offer

import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.presentation.viewmodel.BaseView

/**
 * This class is a view in MVVM to define the Offer callback
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
interface OfferView : BaseView {
    fun onOfferLoaded(offer: Offer?)
}