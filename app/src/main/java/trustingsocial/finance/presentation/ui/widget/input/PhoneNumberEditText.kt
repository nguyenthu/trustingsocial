package trustingsocial.finance.presentation.ui.widget.input

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import trustingsocial.finance.utils.Utils

/**
 * This EditText for special phone number
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
class PhoneNumberEditText : CustomEditText {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        // Only number for this field
        inputText.inputType = InputType.TYPE_CLASS_NUMBER

        // Permit maximum length of 11
        inputText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(11))
    }


    /**
     * Check the phone number with right format
     */
    override fun isValid(): Boolean {
        return Utils.checkPhoneNumber(getInputText())
    }
}