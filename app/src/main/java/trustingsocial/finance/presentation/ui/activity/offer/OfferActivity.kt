package trustingsocial.finance.presentation.ui.activity.offer

import android.content.Intent
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_offer.*
import trustingsocial.finance.App
import trustingsocial.finance.R
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.databinding.ActivityOfferBinding
import trustingsocial.finance.domain.usecase.GetOfferUseCase
import trustingsocial.finance.extension.formatMoneyVND
import trustingsocial.finance.presentation.ui.activity.BaseActivity
import trustingsocial.finance.presentation.ui.activity.loan.LoanActivity
import javax.inject.Inject

/**
 * This activity is to process the offer info for a customer
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
class OfferActivity : BaseActivity<ActivityOfferBinding, OfferViewModel, OfferComponent>(), OfferView {

    /**
     * Define the main resource id for the layout
     */
    override fun getLayoutId(): Int {
        return R.layout.activity_offer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setup click for button begin
        btnBegin.setOnClickListener { startActivity(Intent(this, LoanActivity::class.java)) }

        // Get the offer
        viewModel?.getOffer()

        viewDataBinding.viewModel = viewModel
    }
    /**
     * Get the ViewModel of Offer
     */
    override fun getViewModelClass(): Class<OfferViewModel>? {
        return OfferViewModel::class.java
    }
    /**
     * Attach the UseCase to view model
     */
    @Inject
    fun setViewModelAttribute(offerUseCase: GetOfferUseCase) {
        viewModel?.setAttributes(offerUseCase)
    }

    /**
     * Create a dagger component for Loan
     */
    override fun createComponent(): OfferComponent? {
        return DaggerOfferComponent.builder()
            .applicationComponent(App.get(this@OfferActivity)!!.component())
            .build()
    }

    /**
     * Inject the component for this activity
     */
    override fun onInject(component: OfferComponent) {
        component.inject(this@OfferActivity)
    }

    /**
     * Load an offer to UI
     */
    override fun onOfferLoaded(offer: Offer?) {
        // Bind the result to view
        Glide.with(this).load(offer?.bank!!.logo).into(ivBank)
        tvBank.text = offer.bank.name
        tvLimit.text = String.format(getString(R.string.offer_limit_info), offer.minAmount.formatMoneyVND(), offer.maxAmount.formatMoneyVND())
        tvDeadline.text = String.format(getString(R.string.offer_deadline_info), offer.minTenor, offer.maxTenor)
        tvInterest.text = String.format(getString(R.string.offer_interest_info), offer.interestRate)
    }
}