package trustingsocial.finance.utils

/**
 * This Utility class for providing some basic methods
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
class Utils {
    companion object {

        /**
         * Check the phone number with right format
         * using the regular expression for a phone
         *
         * @param phoneNumber
         */
        fun checkPhoneNumber(phoneNumber: String?): Boolean {
            if (phoneNumber != null) {
                val phoneRegularExpression =
                    "^(012\\d|016[2-9]|018[68]|0199|08[689]|09\\d)\\d{7}$"
                return phoneRegularExpression.toRegex().matches(phoneNumber)
            }
            return false
        }

        /**
         * Check the id number
         * ensure that the length of 9 or 12
         */
        fun checkIdNumber(idNumber: String?): Boolean {
            if (idNumber != null) {
                val idNumberExpression = "\\d{9}|\\d{12}"
                return idNumberExpression.toRegex().matches(idNumber)
            }
            return false
        }
    }
}