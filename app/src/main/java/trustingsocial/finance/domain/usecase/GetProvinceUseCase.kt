package trustingsocial.finance.domain.usecase

import io.reactivex.Single
import trustingsocial.finance.data.remote.response.ProvincesResponse
import trustingsocial.finance.data.repository.TrustingSocialRepository
import javax.inject.Inject

/**
 * This interface is to process the use case of getting province list
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
class GetProvinceUseCase @Inject constructor(private val trustingSocialRepository: TrustingSocialRepository){
    fun getProvinceList() : Single<ProvincesResponse> {
        return trustingSocialRepository.getProvinceList()
    }
}
