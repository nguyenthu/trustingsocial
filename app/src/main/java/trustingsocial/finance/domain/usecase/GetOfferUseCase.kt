package trustingsocial.finance.domain.usecase

import io.reactivex.Single
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.repository.TrustingSocialRepository
import javax.inject.Inject

/**
 * This interface is to process the use case of getting offer
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
class GetOfferUseCase @Inject constructor(private val trustingSocialRepository: TrustingSocialRepository){
    fun getOffer() : Single<Offer>{
        return trustingSocialRepository.getOffer()
    }
}