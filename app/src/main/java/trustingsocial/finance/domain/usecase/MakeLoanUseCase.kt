package trustingsocial.finance.domain.usecase

import io.reactivex.Single
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.repository.TrustingSocialRepository
import javax.inject.Inject

/**
 * This interface is to process the use case of making a loan
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
class MakeLoanUseCase @Inject constructor(private val trustingSocialRepository: TrustingSocialRepository){
    fun makeLoans(phoneNumber: String, fullName: String,
                  nationalIdNumber: String, monthlyIncome: Long, province: String) : Single<Loans> {
        return trustingSocialRepository.makeLoans(phoneNumber, fullName, nationalIdNumber, monthlyIncome, province)
    }
}
