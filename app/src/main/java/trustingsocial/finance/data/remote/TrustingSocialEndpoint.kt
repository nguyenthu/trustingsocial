package trustingsocial.finance.data.remote

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.request.LoansRequest
import trustingsocial.finance.data.remote.response.ProvincesResponse

/**
 * This interface is to define the endpoint which is requested for getting data
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
interface TrustingSocialEndpoint {
    // For assets/api/offer.json
    @GET("/api/offer")
    fun getOffer(): Single<Offer>

    // For assets/api/provinces.json
    @GET("/api/provinces")
    fun getProvinceList(): Single<ProvincesResponse>

    // For assets/api/loans.json
    @POST("/api/loans")
    fun makeLoans(@Body loansRequest: LoansRequest): Single<Loans>
}