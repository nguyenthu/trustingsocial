package trustingsocial.finance.data.remote.response

/**
 * This class is to keep data of a list of provinces
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 * @param total the count of a returned list
 */
data class ProvincesResponse(var total: Int, var provincesList: ArrayList<String>)