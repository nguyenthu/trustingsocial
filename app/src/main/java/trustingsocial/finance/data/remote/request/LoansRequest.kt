package trustingsocial.finance.data.remote.request

/**
 * This class is to request the body data to make a loan
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 * @param phoneNumber the phone of a user who wants to make a loan
 * @param fullName the full name of a user who wants to make a loan
 * @param nationalIdNumber the id number of a user who wants to make a loan
 * @param monthlyIncome the salary of a user who wants to make a loan
 * @param province the place of a user
 */
data class LoansRequest(
    var phoneNumber: String, var fullName: String,
    var nationalIdNumber: String, var monthlyIncome: Long, var province: String
){
    companion object{
        const val AMOUNT1 = 3000000L
        const val AMOUNT2 = 10000000L
    }

    /**
     * Adjust monthly income value before sent to server
     * Example:
     * - income < 3.000.000 --> income = 1
     * - 3.000.000 <= income < 10.000.000 --> income = 3000001
     * - income >= 10.000.000  --> income = 10.000.001
     */
    fun adjustIncome() : LoansRequest{
        if(monthlyIncome < AMOUNT1){
            monthlyIncome = 1
        }else if(monthlyIncome < AMOUNT2){
            monthlyIncome = AMOUNT1 + 1L
        }else{
            monthlyIncome = AMOUNT2 + 1L
        }
        return this
    }
}