package trustingsocial.finance.data.entity

/**
 * This class is to keep data of a loan
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 * @param id the generated number for this loan
 * @param phoneNumber the phone of a user who has already made a loan
 * @param fullName the full name of a user who has already made a loan
 * @param nationalIdNumber the id number of a user who has already made a loan
 * @param monthlyIncome the salary of a user who has already made a loan
 * @param province the place of a user
 */
data class Loans(
    var id: Int, var phoneNumber: String, var fullName: String,
    var nationalIdNumber: String, var monthlyIncome: Long, var province: String
)