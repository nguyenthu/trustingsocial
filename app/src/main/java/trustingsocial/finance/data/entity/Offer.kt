package trustingsocial.finance.data.entity

/**
 * This class is to keep information about an offer of a bank
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 *
 * @param minAmount the minimum amount in VND that a user can make a loan
 * @param maxAmount the maximum amount in VND that a user can make a loan
 * @param minTenor the minimum duration in months that a user can make a loan
 * @param maxTenor the maximum duration in months that a user can make a loan
 * @param interestRate the rate of interest of a year that a user makes a loan
 * @param bank the bank information that user makes a loan
 */
data class Offer(
    var minAmount: Long, var maxAmount: Long, var minTenor: Int, var maxTenor: Int,
    var interestRate: Double, var bank: Bank
)


/**
 * This class is to describe the bank information
 *
 * @param name the name of a bank, for instance: Vietcombank, Eximbank, Agribank,....
 * @param logo the hyperlink refers to the image of a bank
 */
class Bank(var name: String, var logo: String)