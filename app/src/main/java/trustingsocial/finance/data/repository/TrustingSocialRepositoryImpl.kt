package trustingsocial.finance.data.repository

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.TrustingSocialEndpoint
import trustingsocial.finance.data.remote.request.LoansRequest
import trustingsocial.finance.data.remote.response.ProvincesResponse

/**
 * This class is to implement all methods of repository
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
class TrustingSocialRepositoryImpl(private val trustingSocialEndpoint: TrustingSocialEndpoint) : TrustingSocialRepository{

    /**
     * Get the offer info, call directly from endpoint
     */
    override fun getOffer(): Single<Offer> {
        return trustingSocialEndpoint.getOffer()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Get the province list, call directly from endpoint
     */
    override fun getProvinceList(): Single<ProvincesResponse> {
        return trustingSocialEndpoint.getProvinceList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Make a loan, call directly from endpoint
     *
     * @param phoneNumber phone info of a customer
     * @param fullName name of a customer
     * @param nationalIdNumber id number of a customer
     * @param monthlyIncome the salary of a customer
     * @param province the address of a customer
     */
    override fun makeLoans(
        phoneNumber: String,
        fullName: String,
        nationalIdNumber: String,
        monthlyIncome: Long,
        province: String
    ): Single<Loans> {
        return trustingSocialEndpoint.makeLoans(LoansRequest(phoneNumber, fullName, nationalIdNumber, monthlyIncome, province).adjustIncome())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}