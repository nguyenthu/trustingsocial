package trustingsocial.finance.data.repository

import io.reactivex.Single
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.response.ProvincesResponse

/**
 * This interface is to define all methods for repository of Trusting Social
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
interface TrustingSocialRepository {
    fun getOffer() : Single<Offer>
    fun getProvinceList() : Single<ProvincesResponse>
    fun makeLoans(phoneNumber: String, fullName: String,
                  nationalIdNumber: String, monthlyIncome: Long, province: String) : Single<Loans>
}