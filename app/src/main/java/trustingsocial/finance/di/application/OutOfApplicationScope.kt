package trustingsocial.finance.di.application

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class OutOfApplicationScope