package trustingsocial.finance.di.application

import dagger.Component
import trustingsocial.finance.App
import trustingsocial.finance.data.repository.TrustingSocialRepository
import javax.inject.Singleton

/**
 * This interface is to define the component in Dagger of application
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 */
@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, EndpointModule::class])
interface ApplicationComponent {
    fun inject(app: App)
    fun trustingSocialRepository() : TrustingSocialRepository
}

