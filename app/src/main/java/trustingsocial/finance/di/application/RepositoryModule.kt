package trustingsocial.finance.di.application

import dagger.Module
import dagger.Provides
import trustingsocial.finance.data.remote.TrustingSocialEndpoint
import trustingsocial.finance.data.repository.TrustingSocialRepository
import trustingsocial.finance.data.repository.TrustingSocialRepositoryImpl
import javax.inject.Singleton

/**
 * This class is a module in Dagger to provide some methods in repository feature
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
@Module
class RepositoryModule {

    /**
     * Provide the repository object
     */
    @Provides
    fun provideRepository(trustingSocialEndpoint: TrustingSocialEndpoint): TrustingSocialRepository {
        return TrustingSocialRepositoryImpl(trustingSocialEndpoint)
    }
}
