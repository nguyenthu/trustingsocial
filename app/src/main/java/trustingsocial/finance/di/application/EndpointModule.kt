package trustingsocial.finance.di.application

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import trustingsocial.finance.data.remote.TrustingSocialEndpoint
import javax.inject.Singleton

/**
 * This class is a module in Dagger to provide some methods in endpoint
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
@Module
class EndpointModule {

    /**
     * Provide the endpoint instance
     */
    @Provides
    @Singleton
    fun provideTrustingSocialEndpoint(retrofit: Retrofit): TrustingSocialEndpoint {
        return retrofit
            .create(TrustingSocialEndpoint::class.java)
    }
}
