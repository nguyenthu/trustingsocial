package trustingsocial.finance.di.application

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ir.mirrajabi.okhttpjsonmock.OkHttpMockInterceptor
import ir.mirrajabi.okhttpjsonmock.providers.InputStreamProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import trustingsocial.finance.App
import trustingsocial.finance.BuildConfig
import java.io.InputStream
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import java.io.IOException

/**
 * This class is a module in Dagger to provide some methods in application scope
 *
 * @since 2019/06/27
 * @author Thu Nguyen
 *
 */
@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return application
    }

    /**
     * Provide the http logging interceptor
     * and only show log in debug mode
     *
     */
    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor() : HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    /**
     * Provide the OkHttpClient object
     *
     * @param app the Application object
     * @param interceptor for showing the log data while calling api
     */
    @Provides
    @Singleton
    fun provideOkHttpClient(app: Application, interceptor: HttpLoggingInterceptor): OkHttpClient {
        val okBuilder = OkHttpClient.Builder()
        okBuilder.addInterceptor(interceptor)
        okBuilder.addInterceptor { chain ->
            val request = chain.request()
            val builder = request.newBuilder()
            builder.addHeader("Content-Type", "application/json")
            chain.proceed(builder.build())
        }
        okBuilder.addInterceptor(OkHttpMockInterceptor(CustomInputStreamProvider(app), 5))
        okBuilder.connectTimeout(60, TimeUnit.SECONDS)
        okBuilder.readTimeout(60, TimeUnit.SECONDS)
        okBuilder.writeTimeout(60, TimeUnit.SECONDS)

        return okBuilder.build()
    }

    /**
     * Provide the retrofit object
     *
     * @param okHttpClient
     * @param gson The Gson object to serialize and deserialize the request and response
     */
    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://example.com/")
            .client(okHttpClient)
            .build()
    }

    /**
     * Provide the Gson object ignoring the final, transient and static modifier
     *
     * @return Gson object
     */
    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    /**
     * This class is to define the input stream read from assets folder
     */
    class CustomInputStreamProvider(private var app: Application) : InputStreamProvider{
        override fun provide(path: String?): InputStream? {
            try {
                return app.assets.open(path!!)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

    }
}
