package trustingsocial.finance.extension

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * Format a double to display money as string
 * with the group separator "."
 *
 * @return the formatted money, example: value = 10000 --> result = 10.000
 */
fun Double.formatMoneyVND(): String {
    return formatMoney(this)
}

/**
 * Format a double to display money as string
 * with the group separator "."
 *
 * @return the formatted money, example: value = 10000 --> result = 10.000
 */
fun Long.formatMoneyVND(): String {
    return formatMoney(this)
}

/**
 * Format a money value to string
 */
fun formatMoney(any: Any) : String{
    val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
    val symbols = formatter.decimalFormatSymbols
    symbols.groupingSeparator = '.'
    formatter.maximumFractionDigits = 0
    formatter.roundingMode = RoundingMode.HALF_UP
    formatter.decimalFormatSymbols = symbols
    return formatter.format(any)
}