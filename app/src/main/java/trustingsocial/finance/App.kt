package trustingsocial.finance

import android.app.Activity
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import trustingsocial.finance.di.application.*

/**
 * This is the Application class
 * it is for initializing the component, inject Dagger
 *
 * @since 2019/06/25
 * @author Thu Nguyen
 */
class App : MultiDexApplication() {
    lateinit var component: ApplicationComponent
    companion object{
        fun get(activity: Activity): App? {
            return App::class.java.cast(activity.application)
        }
    }
    override fun onCreate() {
        super.onCreate()

        // Enable multidex
        MultiDex.install(this)

        // Inject application component
        inject()
    }

    fun component(): ApplicationComponent? {
        return component
    }

    /**
     * Create dagger component and inject to
     */
    private fun inject() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .repositoryModule(RepositoryModule())
            .endpointModule(EndpointModule())
            .build()
        component.inject(this)
    }
}