package trustingsocial.finance.di

import dagger.Component
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.repository.TrustingSocialRepository
import trustingsocial.finance.di.application.ApplicationModule
import trustingsocial.finance.di.application.EndpointModule
import trustingsocial.finance.di.application.RepositoryModule
import javax.inject.Singleton

/**
 * This is to setup the App Component for testing
 * @author Thu Nguyen
 */
@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, EndpointModule::class])
interface ApplicationTestComponent {
    fun inject(baseTest: BaseTest)
    fun trustingSocialRepository() : TrustingSocialRepository
}