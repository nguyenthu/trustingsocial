package trustingsocial.finance

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.robolectric.RuntimeEnvironment
import trustingsocial.finance.di.ApplicationTestComponent
import trustingsocial.finance.di.DaggerApplicationTestComponent
import trustingsocial.finance.di.application.ApplicationModule
import trustingsocial.finance.di.application.EndpointModule
import trustingsocial.finance.di.application.RepositoryModule

/**
 * This is the base test class, used and applied for all children
 * @author Thu Nguyen
 */
abstract class BaseTest {
    lateinit var testAppComponent: ApplicationTestComponent

    @Before
    open fun setUp(){
        this.configureDi()
        // Init schedule handler
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    /**
     * Configure the dependency injection
     */
    open fun configureDi(){
        this.testAppComponent = DaggerApplicationTestComponent.builder()
            .applicationModule(ApplicationModule(RuntimeEnvironment.application))
            .repositoryModule(RepositoryModule())
            .endpointModule(EndpointModule())
            .build()
    }
}