package trustingsocial.finance.utils

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * This class is to test the Utils.kt class
 * @since 2019/06/27
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(manifest=Config.NONE)
class UtilsTest{

    /**
     * Check the param phone number is null
     */
    @Test
    fun `test check phone number null`(){
        Assert.assertFalse(Utils.checkPhoneNumber(null))
    }

    /**
     * Check the param phone number empty
     */
    @Test
    fun `test check phone number empty`(){
        Assert.assertFalse(Utils.checkPhoneNumber(""))
    }

    /**
     * Check the phone number wrong format, less than 10 digit
     */
    @Test
    fun `test check phone number less than 10 digit`(){
        Assert.assertFalse(Utils.checkPhoneNumber("025847589"))
        Assert.assertFalse(Utils.checkPhoneNumber("090870836"))
    }

    /**
     * Check the phone number wrong format, contains a letter
     */
    @Test
    fun `test check phone number contains a letter`(){
        Assert.assertFalse(Utils.checkPhoneNumber("090870836a"))
        Assert.assertFalse(Utils.checkPhoneNumber("0120589652b"))
    }

    /**
     * Check the phone number right format
     * "^(012\\d|016[2-9]|018[68]|0199|08[689]|09\\d)\\d{7}$"
     */
    @Test
    fun `test check phone number correctly`(){
        Assert.assertTrue(Utils.checkPhoneNumber("01203698741"))
        Assert.assertTrue(Utils.checkPhoneNumber("01293620589"))
        Assert.assertTrue(Utils.checkPhoneNumber("01625698458"))
        Assert.assertTrue(Utils.checkPhoneNumber("01696985474"))
        Assert.assertTrue(Utils.checkPhoneNumber("01862583698"))
        Assert.assertTrue(Utils.checkPhoneNumber("01882596368"))
        Assert.assertTrue(Utils.checkPhoneNumber("01993693652"))
        Assert.assertTrue(Utils.checkPhoneNumber("0869554555"))
        Assert.assertTrue(Utils.checkPhoneNumber("0899999999"))
        Assert.assertTrue(Utils.checkPhoneNumber("0902596852"))
        Assert.assertTrue(Utils.checkPhoneNumber("0991258695"))
    }

    /**
     * Check the param id number is null
     */
    @Test
    fun `test id number null`(){
        Assert.assertFalse(Utils.checkIdNumber(null))
    }

    /**
     * Check the param id number empty
     */
    @Test
    fun `test check id number empty`(){
        Assert.assertFalse(Utils.checkIdNumber(""))
    }

    /**
     * Check the param id number wrong format, contains a letter
     */
    @Test
    fun `test check id number contain a letter`(){
        Assert.assertFalse(Utils.checkIdNumber("23568956a"))
    }

    /**
     * Check the param id number wrong format, neither 9 nor 12 digits
     */
    @Test
    fun `test check id number neither 9 nor 12`(){
        Assert.assertFalse(Utils.checkIdNumber("123"))
        Assert.assertFalse(Utils.checkIdNumber("12345678"))
        Assert.assertFalse(Utils.checkIdNumber("1234567890"))
        Assert.assertFalse(Utils.checkIdNumber("12345678901"))
    }

    /**
     * Check the param id number right format, 9 or 12 digits
     */
    @Test
    fun `test check id number right format`(){
        Assert.assertTrue(Utils.checkIdNumber("123456789"))
        Assert.assertTrue(Utils.checkIdNumber("123456789123"))
    }
}