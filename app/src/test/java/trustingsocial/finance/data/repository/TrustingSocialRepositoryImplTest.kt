package trustingsocial.finance.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.response.ProvincesResponse
import javax.inject.Inject

/**
 * This class to test the repository implementation
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class TrustingSocialRepositoryImplTest : BaseTest(){
    lateinit var trustingSocialRepository: TrustingSocialRepository

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()
        trustingSocialRepository = testAppComponent.trustingSocialRepository()
    }

    @Test
    fun `test get offer success`(){
        val testObserver = TestObserver<Offer>()
        trustingSocialRepository.getOffer()
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val offer = testObserver.values()[0]
            Assert.assertTrue(offer.minAmount > 0)
            Assert.assertTrue(offer.maxAmount > 0)
            Assert.assertTrue(offer.minTenor > 0)
            Assert.assertTrue(offer.maxTenor > 0)
            Assert.assertTrue(offer.bank.name.isNotBlank())
            Assert.assertTrue(offer.bank.logo.isNotBlank())
        } else{
            assert(true)
        }
    }

    @Test
    fun `test get province success`(){
        val testObserver = TestObserver<ProvincesResponse>()
        trustingSocialRepository.getProvinceList()
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val provinceResponse = testObserver.values()[0]
            Assert.assertTrue(provinceResponse.total > 0)
            Assert.assertTrue(provinceResponse.provincesList.size > 0)
        } else{
            assert(true)
        }
    }

    @Test
    fun `test making loan success even empty field`(){
        val testObserver = TestObserver<Loans>()
        trustingSocialRepository.makeLoans("", "", "", 0, "")
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val loans = testObserver.values()[0]
            Assert.assertTrue(loans.id > 0)
            Assert.assertTrue(loans.fullName.isNotBlank())
            Assert.assertTrue(loans.phoneNumber.isNotBlank())
            Assert.assertTrue(loans.province.isNotBlank())
            Assert.assertTrue(loans.nationalIdNumber.isNotBlank())
        } else{
            assert(true)
        }
    }

    @Test
    fun `test making loan success`(){
        val testObserver = TestObserver<Loans>()
        trustingSocialRepository.makeLoans("0989987678", "Nguyen Van A", "198899999"
            , 3000001, "An Giang")
            .subscribe(testObserver)
        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val loans = testObserver.values()[0]
            Assert.assertTrue(loans.id > 0)
            Assert.assertTrue(loans.fullName.isNotBlank())
            Assert.assertTrue(loans.phoneNumber.isNotBlank())
            Assert.assertTrue(loans.province.isNotBlank())
            Assert.assertTrue(loans.nationalIdNumber.isNotBlank())
        } else{
            assert(true)
        }
    }
}