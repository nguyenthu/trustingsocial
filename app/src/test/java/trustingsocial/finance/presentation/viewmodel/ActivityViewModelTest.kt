package trustingsocial.finance.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import io.reactivex.disposables.Disposable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest

/**
 * This class to test the base view model class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(manifest=Config.NONE)
class ActivityViewModelTest : BaseTest(){
    lateinit var activityViewModel: ActivityViewModel

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()
        MockitoAnnotations.initMocks(this)

        activityViewModel = ActivityViewModel()
    }

    /**
     * Test the view() function success
     */
    @Test
    fun `test view() function`(){
        val testBaseView = TestBaseView()
        activityViewModel.onAttached(testBaseView)
        Assert.assertTrue(activityViewModel.view<TestBaseView>() == testBaseView)
    }

    /**
     * Test the onAttach() function success
     */
    @Test
    fun `test onAttached() function`(){
        val testBaseView = TestBaseView()
        activityViewModel.onAttached(testBaseView)

        Assert.assertTrue(activityViewModel.mViewWeakReference?.get() == testBaseView)
    }

    /**
     * Test the onCreate() function success
     */
    @Test
    fun `test onCreated() function`(){
        activityViewModel.onCreated()

        Assert.assertTrue(activityViewModel.compositeDisposables != null)
    }

    /**
     * Test the onDestroy() function success
     */
    @Test
    fun `test onDestroy() function`(){
        activityViewModel.onCreated()

        // Call onDestroy() to test
        activityViewModel.onDestroy()

        Assert.assertTrue(activityViewModel.compositeDisposables?.size()!! == 0)
    }

    /**
     * Test the addDisposable() function success
     */
    @Test
    fun `test addDisposable() function`(){
        activityViewModel.onCreated()

        // Call addDisposable() to test
        activityViewModel.addDisposable(TestDisposable())
        Assert.assertTrue(activityViewModel.compositeDisposables?.size()!! > 0)
    }
    /**
     * Mock for test
     */
    class TestBaseView : BaseView{
        override fun lifecycleOwner(): LifecycleOwner {
            return Mockito.mock(LifecycleOwner::class.java)
        }

    }

    /**
     * Mock for test
     */
    class TestDisposable : Disposable{
        override fun isDisposed(): Boolean {
            return false
        }

        override fun dispose() {
        }

    }
}