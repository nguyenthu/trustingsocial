package trustingsocial.finance.presentation.ui.activity.offer

import android.os.Handler
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.android.synthetic.main.activity_offer.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.presentation.ui.activity.loan.LoanActivity
import org.robolectric.Shadows.shadowOf
import trustingsocial.finance.R


/**
 * This class to test the Offer Activity class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class OfferActivityTest : BaseTest(){
    private lateinit var offerActivity: OfferActivity
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp(){
        offerActivity = Robolectric.buildActivity(OfferActivity::class.java).setup().get()
    }

    /**
     * Test that the Offer Activity inflate resource correctly
     */
    @Test
    fun testInflateResourceLayout(){
        Assert.assertTrue(offerActivity.getLayoutId() == R.layout.activity_offer)
    }

    /**
     * Test the view model class for setting
     */
    @Test
    fun testViewModelClass(){
        Assert.assertTrue(offerActivity.getViewModelClass() == OfferViewModel::class.java)
    }

    /**
     * Test set attributes for model
     */
    @Test
    fun testSettingAttributeForModel(){
        Assert.assertTrue(offerActivity.viewModel?.getOfferUseCase != null)
    }

    /**
     * Ensure that when click the button will navigate to Loan Activity
     */
    @Test
    fun testClickButton(){
        offerActivity.btnBegin.performClick()
        val startedIntent = shadowOf(offerActivity).getNextStartedActivity()
        val shadowIntent = shadowOf(startedIntent)
        Assert.assertEquals(LoanActivity::class.java, shadowIntent.getIntentClass())
    }
}