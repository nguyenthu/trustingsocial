package trustingsocial.finance.presentation.ui.activity.loan

import android.os.Handler
import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.android.synthetic.main.activity_loans.*
import kotlinx.android.synthetic.main.custom_dropdown.view.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import org.robolectric.Shadows
import trustingsocial.finance.R


/**
 * This class to test the Loan Activity class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class LoanActivityTest : BaseTest(){
    companion object {
        const val PHONE_NUMBER_VALID = "0908745989"
        const val ID_NUMBER_VALID = "909087656787"
        const val FULL_NAME_VALID = "Nguyen Van A"
        const val INCOME_VALID = "3000000"
        const val ADDRESS_VALID = "An Giang"
    }
    private lateinit var loanActivity: LoanActivity
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp(){
        loanActivity = Robolectric.buildActivity(LoanActivity::class.java).setup().get()
    }

    /**
     * Test that the Loan Activity inflate resource correctly
     */
    @Test
    fun testInflateResourceLayout(){
        Assert.assertTrue(loanActivity.getLayoutId() == R.layout.activity_loans)
    }

    /**
     * Test the view model class for setting
     */
    @Test
    fun testViewModelClass(){
        Assert.assertTrue(loanActivity.getViewModelClass() == LoanViewModel::class.java)
    }

    /**
     * Test set attributes for model
     */
    @Test
    fun testSettingAttributeForModel(){
        Assert.assertTrue(loanActivity.viewModel?.getProvinceUseCase != null)
        Assert.assertTrue(loanActivity.viewModel?.makeLoanUseCase != null)
    }

    /**
     * Test validateInput() failed due to the phone number is wrong format
     */
    @Test
    fun testValidateInputFailedPhoneNumber(){
        loanActivity.etPhoneNumber.inputText.setText("123")
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText(INCOME_VALID)
        loanActivity.ddAddress.tvTitle.setText(ADDRESS_VALID)

        Assert.assertFalse(loanActivity.validateInput())
    }

    /**
     * Test validateInput() failed due to the full name is empty
     */
    @Test
    fun testValidateInputFailedFullName(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText("")
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText(INCOME_VALID)
        loanActivity.ddAddress.tvTitle.setText(ADDRESS_VALID)

        Assert.assertFalse(loanActivity.validateInput())
    }

    /**
     * Test validateInput() failed due to the id number is wrong format
     */
    @Test
    fun testValidateInputFailedIdNumber(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText("123")
        loanActivity.etIncome.inputText.setText(INCOME_VALID)
        loanActivity.ddAddress.tvTitle.setText(ADDRESS_VALID)

        Assert.assertFalse(loanActivity.validateInput())
    }

    /**
     * Test validateInput() failed due to income value is less than 3.000.000
     */
    @Test
    fun testValidateInputFailedIncome(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText("2999999")
        loanActivity.ddAddress.tvTitle.text = ADDRESS_VALID

        Assert.assertFalse(loanActivity.validateInput())
    }
    /**
     * Test validateInput() failed due to address is empty
     */
    @Test
    fun testValidateInputFailedAddress(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText(INCOME_VALID)

        Assert.assertFalse(loanActivity.validateInput())
    }
    /**
     * Test validateInput() success all
     */
    @Test
    fun testValidateInputSuccess(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText(INCOME_VALID)
        loanActivity.ddAddress.tvTitle.text = ADDRESS_VALID

        Assert.assertTrue(loanActivity.validateInput())
    }

    /**
     * Test click button when all inputs are invalid format
     * so show the error
     */
    @Test
    fun testClickButtonWhenInvalidInput(){
        loanActivity.etPhoneNumber.inputText.setText("12345")
        loanActivity.etFullName.inputText.setText("")
        loanActivity.etIdNumber.inputText.setText("2345")
        loanActivity.etIncome.inputText.setText("29000")

        // Perform click on a button
        loanActivity.btSubmit.performClick()

        // Check the error
        Assert.assertEquals(loanActivity.getString(R.string.phone_number_format_error), loanActivity.etPhoneNumber.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.full_name_empty_error), loanActivity.etFullName.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.id_number_format_error), loanActivity.etIdNumber.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.salary_limit_error), loanActivity.etIncome.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.address_empty_error), loanActivity.ddAddress.tvError.text)
    }

    /**
     * Test click button when all inputs are empty
     * so show the error
     */
    @Test
    fun testClickButtonWhenEmptyInput(){
        loanActivity.etPhoneNumber.inputText.setText("")
        loanActivity.etFullName.inputText.setText("")
        loanActivity.etIdNumber.inputText.setText("")
        loanActivity.etIncome.inputText.setText("")

        // Perform click on a button
        loanActivity.btSubmit.performClick()

        // Check the error
        Assert.assertEquals(loanActivity.getString(R.string.phone_number_empty_error), loanActivity.etPhoneNumber.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.full_name_empty_error), loanActivity.etFullName.tvError.text)
        Assert.assertEquals("", loanActivity.etIdNumber.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.salary_empty_error), loanActivity.etIncome.tvError.text)
        Assert.assertEquals(loanActivity.getString(R.string.address_empty_error), loanActivity.ddAddress.tvError.text)
    }

    /**
     * Test click button when all inputs are valid
     * so show the error
     */
    @Test
    fun testClickButtonValid(){
        loanActivity.etPhoneNumber.inputText.setText(PHONE_NUMBER_VALID)
        loanActivity.etFullName.inputText.setText(FULL_NAME_VALID)
        loanActivity.etIdNumber.inputText.setText(ID_NUMBER_VALID)
        loanActivity.etIncome.inputText.setText(INCOME_VALID)
        loanActivity.ddAddress.tvTitle.text = ADDRESS_VALID

        // Perform click on a button
        loanActivity.btSubmit.performClick()

        // Check the error empty
        Assert.assertEquals("", loanActivity.etPhoneNumber.tvError.text)
        Assert.assertEquals("", loanActivity.etFullName.tvError.text)
        Assert.assertEquals("", loanActivity.etIdNumber.tvError.text)
        Assert.assertEquals("", loanActivity.etIncome.tvError.text)
        Assert.assertTrue(loanActivity.ddAddress.tvError.visibility != View.VISIBLE)
    }

    /**
     * Check that onLoadMade() call will destroy this activity
     */
    @Test
    fun testOnLoadMadeCall(){
        loanActivity.onLoanMade()

        // Check that submit success and close this screen
        Assert.assertTrue(loanActivity.isFinishing || loanActivity.isDestroyed)
    }

    /**
     * Check that non-empty return of province list
     */
    @Test
    fun testOnProvinceLoadedNotEmpty(){
        val provinceList = ArrayList<String>()
        provinceList.add(ADDRESS_VALID)

        loanActivity.onProvinceLoaded(provinceList)

        // Check that the list is also empty
        Assert.assertTrue(loanActivity.ddAddress.listItem.isNotEmpty())
    }
}