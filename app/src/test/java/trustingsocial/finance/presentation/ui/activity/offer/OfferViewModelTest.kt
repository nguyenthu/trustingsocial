package trustingsocial.finance.presentation.ui.activity.offer

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.domain.usecase.GetOfferUseCase
import java.lang.ref.WeakReference

/**
 * This class to test the offer view model class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class OfferViewModelTest : BaseTest(){
    lateinit var offerViewModel: OfferViewModel

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()

        offerViewModel = OfferViewModel()
    }

    @Test
    fun `test setting attribute`(){
        val getOfferUserCase = GetOfferUseCase(testAppComponent.trustingSocialRepository())
        // Call set attribute method
        offerViewModel.setAttributes(getOfferUserCase)

        // Ensure that the setting method correctly
        Assert.assertTrue(offerViewModel.getOfferUseCase == getOfferUserCase)
    }

    @Test
    fun `test get province list`(){
        offerViewModel.setAttributes(GetOfferUseCase(testAppComponent.trustingSocialRepository()))

        // Call get offer method
        val testOfferView = TestOfferView()
        offerViewModel.mViewWeakReference = WeakReference(testOfferView)
        offerViewModel.getOffer()

        // Check that the onProvinceLoaded has called
        Assert.assertTrue(testOfferView.getOfferCallBack)
    }

    /**
     * Implement LoanView for testing
     */
    class TestOfferView : OfferView{
        var getOfferCallBack = false
        override fun onOfferLoaded(offer: Offer?) {
            getOfferCallBack = offer != null
        }

        override fun lifecycleOwner(): LifecycleOwner {
            return Mockito.mock(LifecycleOwner::class.java)
        }
    }
}