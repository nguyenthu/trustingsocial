package trustingsocial.finance.presentation.ui.activity.loan

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.domain.usecase.GetProvinceUseCase
import trustingsocial.finance.domain.usecase.MakeLoanUseCase
import java.lang.ref.WeakReference

/**
 * This class to test the loan view model class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class LoanViewModelTest : BaseTest(){
    lateinit var loanViewModel: LoanViewModel

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()

        loanViewModel = LoanViewModel()
    }

    @Test
    fun `test setting attribute`(){
        val getProvinceUseCase = GetProvinceUseCase(testAppComponent.trustingSocialRepository())
        val makeLoanUseCase = MakeLoanUseCase(testAppComponent.trustingSocialRepository())
        // Call set attribute method
        loanViewModel.setAttributes(getProvinceUseCase, makeLoanUseCase)

        // Ensure that the setting method correctly
        Assert.assertTrue(loanViewModel.getProvinceUseCase == getProvinceUseCase)
        Assert.assertTrue(loanViewModel.makeLoanUseCase == makeLoanUseCase)
    }

    @Test
    fun `test get province list`(){
        loanViewModel.setAttributes(GetProvinceUseCase(testAppComponent.trustingSocialRepository()), MakeLoanUseCase(testAppComponent.trustingSocialRepository()))
        val testLoanView = TestLoanView()
        loanViewModel.mViewWeakReference = WeakReference(testLoanView)
        loanViewModel.getProvinceList()

        // Check that the onProvinceLoaded has called
        Assert.assertTrue(testLoanView.provinceListCallback)
    }

    @Test
    fun `test make loan success even empty`(){
        loanViewModel.setAttributes(GetProvinceUseCase(testAppComponent.trustingSocialRepository()), MakeLoanUseCase(testAppComponent.trustingSocialRepository()))

        // Call process loan method
        val testLoanView = TestLoanView()
        loanViewModel.mViewWeakReference = WeakReference(testLoanView)
        loanViewModel.processLoan("", "", "", 0, "")

        // Check that the onProvinceLoaded has called
        Assert.assertTrue(testLoanView.makeLoanCallback)
    }

    @Test
    fun `test make loan success`(){
        loanViewModel.setAttributes(GetProvinceUseCase(testAppComponent.trustingSocialRepository()), MakeLoanUseCase(testAppComponent.trustingSocialRepository()))

        // Call process loan method
        val testLoanView = TestLoanView()
        loanViewModel.mViewWeakReference = WeakReference(testLoanView)
        loanViewModel.processLoan("0989876789", "Nguyen Van A", "897767788", 10000000, "An Giang")

        // Check that the onProvinceLoaded has called
        Assert.assertTrue(testLoanView.makeLoanCallback)
    }
    /**
     * Implement LoanView for testing
     */
    class TestLoanView : LoanView{

        var provinceListCallback = false
        var makeLoanCallback = false
        override fun onLoanMade() {
            makeLoanCallback = true
        }

        override fun onProvinceLoaded(provinceList: ArrayList<String>?) {
            provinceListCallback = provinceList?.isNotEmpty()!!
        }

        override fun lifecycleOwner(): LifecycleOwner {
            return Mockito.mock(LifecycleOwner::class.java)
        }
    }
}