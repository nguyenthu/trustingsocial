package trustingsocial.finance.extension

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * This class is to test the Extension.kt class
 * @since 2019/06/27
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(manifest=Config.NONE)
class ExtensionTest{

    /**
     * Check money format for Double extension
     */
    @Test
    fun `test check money format of Double extension`(){
        Assert.assertTrue(0.0.formatMoneyVND() == "0")
        Assert.assertTrue(999.0.formatMoneyVND() == "999")
        Assert.assertTrue(120.0.formatMoneyVND() == "120")
        Assert.assertTrue(1000.0.formatMoneyVND() == "1.000")
        Assert.assertTrue(10000.0.formatMoneyVND() == "10.000")
        Assert.assertTrue(1000000.0.formatMoneyVND() == "1.000.000")
    }

    /**
     * Check money format for Long extension
     */
    @Test
    fun `test check money format of Long extension`(){
        Assert.assertTrue(0L.formatMoneyVND() == "0")
        Assert.assertTrue(999L.formatMoneyVND() == "999")
        Assert.assertTrue(120L.formatMoneyVND() == "120")
        Assert.assertTrue(1000L.formatMoneyVND() == "1.000")
        Assert.assertTrue(10000L.formatMoneyVND() == "10.000")
        Assert.assertTrue(1000000L.formatMoneyVND() == "1.000.000")
    }

    /**
     * Check money format
     */
    @Test
    fun `test check money format`(){
        Assert.assertTrue(formatMoney(0.0) == "0")
        Assert.assertTrue(formatMoney(0L) == "0")
        Assert.assertTrue(formatMoney(999.0) == "999")
        Assert.assertTrue(formatMoney(999L) == "999")
        Assert.assertTrue(formatMoney(1000.0) == "1.000")
        Assert.assertTrue(formatMoney(1000L) == "1.000")
        Assert.assertTrue(formatMoney(10000.0) == "10.000")
        Assert.assertTrue(formatMoney(10000L) == "10.000")
        Assert.assertTrue(formatMoney(1000000.0) == "1.000.000")
        Assert.assertTrue(formatMoney(1000000L) == "1.000.000")
    }
}