package trustingsocial.finance.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.response.ProvincesResponse
import javax.inject.Inject

/**
 * This class to test the getting province use case class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class GetProvinceUseCaseTest : BaseTest(){
    lateinit var getProvinceUseCase: GetProvinceUseCase

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()
        getProvinceUseCase = GetProvinceUseCase(testAppComponent.trustingSocialRepository())
    }

    @Test
    fun `test get province use case success`(){
        val testObserver = TestObserver<ProvincesResponse>()
        getProvinceUseCase.getProvinceList()
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val provinceResponse = testObserver.values()[0]
            Assert.assertTrue(provinceResponse.total > 0)
            Assert.assertTrue(provinceResponse.provincesList.size > 0)
        } else{
            assert(true)
        }
    }
}