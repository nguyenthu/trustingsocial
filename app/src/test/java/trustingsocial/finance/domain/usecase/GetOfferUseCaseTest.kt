package trustingsocial.finance.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.entity.Offer
import javax.inject.Inject

/**
 * This class to test the getting offer use case class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class GetOfferUseCaseTest : BaseTest(){
    lateinit var getOfferUseCase: GetOfferUseCase

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()
        getOfferUseCase = GetOfferUseCase(testAppComponent.trustingSocialRepository())
    }

    @Test
    fun `test get offer use case success`(){
        val testObserver = TestObserver<Offer>()
        getOfferUseCase.getOffer()
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val offer = testObserver.values()[0]
            Assert.assertTrue(offer.minAmount > 0)
            Assert.assertTrue(offer.maxAmount > 0)
            Assert.assertTrue(offer.minTenor > 0)
            Assert.assertTrue(offer.maxTenor > 0)
            Assert.assertTrue(offer.bank.name.isNotBlank())
            Assert.assertTrue(offer.bank.logo.isNotBlank())
        } else{
            assert(true)
        }
    }
}