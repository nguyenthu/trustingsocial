package trustingsocial.finance.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import trustingsocial.finance.BaseTest
import trustingsocial.finance.data.entity.Loans
import trustingsocial.finance.data.entity.Offer
import trustingsocial.finance.data.remote.response.ProvincesResponse
import javax.inject.Inject

/**
 * This class to test making a loan use case class
 *
 * @since 2019/06/28
 * @author Thu Nguyen
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(23), manifest = "AndroidManifest.xml")
class MakeLoanUseCaseTest : BaseTest(){
    lateinit var makeLoanUseCase: MakeLoanUseCase

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @Before
    override fun setUp() {
        super.setUp()
        makeLoanUseCase = MakeLoanUseCase(testAppComponent.trustingSocialRepository())
    }

    @Test
    fun `test making loan use case success`(){
        val testObserver = TestObserver<Loans>()
        makeLoanUseCase.makeLoans("0989987678", "Nguyen Van A", "198899999"
            , 3000001, "An Giang")
            .subscribe(testObserver)

        if(testObserver.valueCount() > 0){
            // Check the result returned
            testObserver.assertNoErrors()

            // Check the content of result
            val loans = testObserver.values()[0]
            Assert.assertTrue(loans.id > 0)
            Assert.assertTrue(loans.fullName.isNotBlank())
            Assert.assertTrue(loans.phoneNumber.isNotBlank())
            Assert.assertTrue(loans.province.isNotBlank())
            Assert.assertTrue(loans.nationalIdNumber.isNotBlank())
        } else{
            assert(true)
        }
    }
}